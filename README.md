# AWS EKS using eksctl
Here is a Tutorial How-To Install AWS EKS Cluster using [**eksctl**](https://eksctl.io/) utility.

Got inspired by [AWS EKS - Create Kubernetes cluster on Amazon EKS | the easy way](https://www.youtube.com/watch?v=p6xDCz00TxU)



### 1. Install awscli
```
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"

apt install -y unzip
unzip awscliv2.zip
sudo ./aws/install

aws --version
```

### 2. Configure awscli
```
aws configure
```

### 3. Install kubectl
```
curl -O https://s3.us-west-2.amazonaws.com/amazon-eks/1.28.5/2024-01-04/bin/linux/amd64/kubectl

chmod +x kubectl

mv kubectl /usr/local/bin/
```

### 4. Install eksctl
```
curl --silent --location "https://github.com/weaveworks/eksctl/releases/latest/download/eksctl_$(uname -s)_amd64.tar.gz" | tar xz -C /tmp
sudo mv /tmp/eksctl /usr/local/bin
```

### 5. Verify
```
eksctl version
```

### 6. Create Cluster
```
eksctl create cluster \
--name test-cluster \
--version 1.28 \
--region us-east-1 \
--nodegroup-name cluster-nodes \
--node-type t3.micro \
--nodes 8
```
Config examples can be found [here](https://github.com/weaveworks/eksctl/tree/main/examples)

_NOTE:_ kubeconfig will be provided at the end of script output

### 7. Delete Cluster
```
eksctl delete cluster --name test-cluster
```

### 8. Check clusters
```
aws eks list-clusters
```

### 9. Generate kubeconfig
```
aws eks --region us-east-1 update-kubeconfig --name garden
```

<br>


## Links
- https://spacelift.io/blog/terraform-eks